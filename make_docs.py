#!/usr/bin/env python

import inspect
import subprocess

import stsb3


def is_relevant(obj):
    """Filter for the inspector to filter out non user defined functions/classes"""
    if hasattr(obj, "__name__") and obj.__name__ == "type":
        return False
    if inspect.isfunction(obj) or inspect.isclass(obj) or inspect.ismethod(obj):
        return True


def get_all_module_docs(top):
    modules = {k: v for k, v in inspect.getmembers(top) if "__" not in k}
    mod_docs = dict()

    for mod_name, mod in modules.items():
        mod_docs[mod_name] = dict()

        for name, data in inspect.getmembers(mod, is_relevant):
            mod_docs[mod_name][name] = [inspect.getdoc(data)]

            if inspect.isclass(data):
                mod_docs[mod_name][name].append(dict())
                for cls_name, cls_data in inspect.getmembers(data, is_relevant):
                    mod_docs[mod_name][name][-1][cls_name] = [inspect.getdoc(cls_data)]

    return mod_docs


def write_mod_docs(mod_docs, fname, for_site=False):
    generated_files = list()

    for module, module_docs in mod_docs.items():
        file_name = fname + module + ".md"
        generated_files.append(file_name)

        with open(file_name, "w") as f:
            f.write(f"# `stsb3.{module}`\n")

            for callable_, callable_docs in module_docs.items():
                if "__" not in callable_:
                    if not for_site:
                        callable_ = callable_.replace("_", "\_")
                    f.write(f"\n## `{callable_}`\n")
                    _doc = callable_docs[0]
                    if not for_site:
                        try:
                            _doc = callable_docs[0].replace("_", "\_")
                        except AttributeError:
                            _doc = callable_docs[0]
                    f.write(f"{_doc}\n")

                    if len(callable_docs) > 1:  # it's a class
                        method_stuff = callable_docs[-1]
                        for method_, method_docs in method_stuff.items():
                            if "__" not in method_:
                                if not for_site:
                                    method_ = method_.replace("_", "\_")
                                f.write(f"\n### `{method_}`\n")
                                _doc = method_docs[0]
                                if not for_site:
                                    try:
                                        _doc = method_docs[0].replace("_", "\_")
                                    except AttributeError:
                                        _doc = method_docs[0]
                                f.write(f"{_doc}\n")
                        f.write("\n")
                    f.write("\n")
            f.write("\n")
    return generated_files


def main():
    local_f = "./docs/"
    # site_f = "/Users/dave/Documents/daviddewhurst.github.io/stsb2/docs/index.md"

    mod_docs = get_all_module_docs(stsb3)
    generated_files = write_mod_docs(mod_docs, local_f, for_site=True)
    for f in generated_files:
        subprocess.check_call(
            [
                "pandoc",
                "--toc",
                "--standalone",
                "--mathjax",
                "-f",
                "markdown",
                "-t",
                "html",
                f,
                "-o",
                f.split(".md")[0] + ".html",
            ]
        )


if __name__ == "__main__":
    main()
