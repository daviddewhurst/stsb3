
import logging

import pytest
import torch
import pyro
import pyro.distributions as dist
from pyro.contrib import autoname

from stsb3 import core, sts, constants, util


def seed_me(seed=0):
    torch.manual_seed(seed)
    pyro.set_rng_seed(seed)


def test_gaussian_noise():
    t1 = 10
    dgp = sts.RandomWalk(t1=t1)
    lik = sts.GaussianNoise(dgp, t1=t1)

    data = dgp()
    assert not lik.cache
    draw = lik()
    lik.data = data
    draw2 = lik()
    assert not torch.equal(draw, draw2)
    logging.info(f"Created likelihood object: {lik}")


def test_poisson_noise():
    t1 = 10
    dgp = sts.RandomWalk(t1=t1).softplus()
    lik = sts.PoissonNoise(dgp, t1=t1)

    data = dgp()
    assert not lik.cache
    draw = lik()
    lik.data = data
    draw2 = lik()
    assert not torch.equal(draw, draw2)
    logging.info(f"Created likelihood object: {lik}")


def test_discriminative_gaussian_noise():
    size = 3
    dims = 4
    time = 5
    X = torch.randn((size, dims, time))
    beta = sts.RandomWalk(t1=time, size=dims, name="beta")
    model = sts.DiscriminativeGaussianNoise(
        beta,
        name="linreg",
        size=size,
        t1=time,
        X=X,
    )
    lr = model()
    assert lr.shape == (size, time)
    logging.info(f"Created likelihood object {model}")


def test_bernoulli_noise():
    t1 = 10
    size = 2
    dgp = sts.AR1(t1=t1, size=size).invlogit()
    lik = sts.BernoulliNoise(dgp, t1=t1, size=size)
    assert torch.equal(lik().unique(), torch.tensor([0.0, 1.0], dtype=torch.float))
    logging.info(f"Created likelihood object {lik}")


def test_random_walk():
    t1 = 10
    rw = sts.RandomWalk(
        name="rw",
        t1=t1,
    )
    assert core._is_pyro_dist(getattr(rw, constants.scale))

    core.redefine(rw, constants.loc, torch.zeros(1))
    core.redefine(rw, constants.scale, torch.ones(1))
    core.redefine(rw, constants.ic, torch.zeros(1))
    samples = torch.stack([rw() for _ in range(2000)])
    # E[rw[n]] -> 0
    # Var(rw[n]) -> n
    mean = samples.squeeze()[..., -1].mean(dim=-1)
    var = samples.squeeze()[..., -1].std(dim=-1).pow(2)
    assert torch.isclose(mean, torch.tensor(0).type(torch.float), atol=1, rtol=0.0)
    assert torch.isclose(var, torch.tensor(t1).type(torch.float), atol=1, rtol=0.0)
    seed_me()
    samples1 = rw()
    rw.has_fast_mode = False
    seed_me()
    samples2 = rw()
    assert samples1.shape == samples2.shape


def test_ccsde():
    t1 = 10
    ccsde = sts.CCSDE(
        name="ccsde",
        t1=t1,
    )
    assert core._is_pyro_dist(getattr(ccsde, constants.scale))

    dt = 0.01
    core.redefine(ccsde, constants.loc, torch.zeros(1))
    core.redefine(ccsde, constants.scale, torch.ones(1))
    core.redefine(ccsde, constants.ic, torch.zeros(1))
    core.redefine(ccsde, constants.dt, dt * torch.ones(1))
    samples = torch.stack([ccsde() for _ in range(2000)])
    # E[rw[n]] -> 0
    # Var(rw[n]) -> dt * n
    mean = samples.squeeze()[..., -1].mean(dim=-1)
    var = samples.squeeze()[..., -1].std(dim=-1).pow(2)
    assert torch.isclose(mean, torch.tensor(0).type(torch.float), atol=1, rtol=0.0)
    assert torch.isclose(var, dt * torch.tensor(t1).type(torch.float), atol=1, rtol=0.0)
    seed_me()
    samples1 = ccsde()
    ccsde.has_fast_mode = False
    seed_me()
    samples2 = ccsde()
    assert samples1.shape == samples2.shape


def test_global_trend():
    t1 = 10
    gt = sts.GlobalTrend(name="gt", t1=t1)
    assert core._is_pyro_dist(getattr(gt, constants.alpha))

    core.redefine(gt, constants.alpha, torch.zeros(1))
    core.redefine(gt, constants.beta, torch.ones(1))
    samples = torch.stack([gt() for _ in range(2000)])
    # E[gt[n]] -> beta * n
    mean = samples.squeeze()[..., -1].mean(dim=-1)
    assert torch.isclose(
        mean, torch.ones(1) * torch.tensor(t1 - 1).type(torch.float), atol=1, rtol=0.0
    )


def test_smooth_seasonal():
    t1 = 10
    ss = sts.SmoothSeasonal(name="ss", t1=t1)
    assert core._is_pyro_dist(getattr(ss, constants.lengthscale))
    assert ss.cycles >= 1  # sensible

    samples = ss()
    assert samples.shape == (1, t1)


def test_ar1():
    t1 = 10
    a = sts.AR1(name="a", t1=t1)
    assert hasattr(a, "has_fast_mode")
    # fast mode is nonfunctional as of 2021-01-30, just calls slow mode
    seed_me()
    samples = a()
    a.has_fast_mode = False
    seed_me()
    samples2 = a()
    assert torch.equal(samples, samples2)


def test_ma1():
    t1 = 10
    m = sts.MA1(name="m", t1=t1)
    trace = pyro.poutine.trace(m).get_trace()
    # even though the tensor created at scale node is one longer in time dimension
    assert trace.nodes["m-loc"]["value"].shape == trace.nodes["m-scale"]["value"].shape


def test_var1():
    dim = 2
    size = 3
    t1 = 4
    var = sts.VAR1(name="v", t1=t1, size=size, dim=dim)
    assert var().shape == (size, dim, t1)
    with pytest.raises(ValueError):
        dim = 1
        var = sts.VAR1(name="v", t1=t1, size=size, dim=dim)
        var()
    dim = 2
    # test shape possibilities -- tracing asserts that they work
    beta = dist.Normal(0.0, 1.0).expand((dim ** 2,))
    var = sts.VAR1(name="v", t1=t1, size=size, dim=dim, beta=beta)
    trace = pyro.poutine.trace(var).get_trace().nodes
    assert trace["v-beta"]["value"].shape == (dim ** 2,)
    beta = dist.Normal(0.0, 1.0).expand(
        (
            size,
            dim ** 2,
        )
    )
    var.beta = beta
    trace = pyro.poutine.trace(var).get_trace().nodes
    assert trace["v-beta"]["value"].shape == (size, dim ** 2)


def test_var1_noise_compose():
    dim = 3
    size = 8
    t1 = 10
    var = sts.VAR1(name="v", t1=t1, size=size, dim=dim)
    gn = sts.GaussianNoise(
        var,
        name="g",
        t1=t1,
        size=size,
        scale=dist.LogNormal(0.0, 1.0).expand((size, dim)).to_event(2),
    )
    v = var()
    g = gn()
    assert v.shape == g.shape
    # note that passing scale with shape (size,) won't work
    scale = dist.LogNormal(0.0, 1.0).expand((size,))
    gn.scale = scale
    with pytest.raises(RuntimeError):
        gn()

    # fitting works with dimension hacks in NoiseBlock
    # NOTE: we are just resetting the scale from the raises test above
    gn.scale = dist.LogNormal(0.0, 1.0).expand((size, dim)).to_event(2)
    data = var()
    gn.data = data
    gn.fit(method_kwargs=dict(niter=2))
    nsamples = 10
    pp = gn.posterior_predictive(nsamples=nsamples)
    assert "dynamic/v-generated" in pp.keys()
    # forecasting works too
    Nt = 5
    fcst = sts.forecast(var, pp, Nt=Nt, nsamples=nsamples)
    assert fcst.shape == (nsamples, size, dim, Nt)


def test_discrete_seasonal():
    t1 = 20
    size = 3
    ds = sts.DiscreteSeasonal(name="ds", t1=t1, size=size)
    # check that it's actually periodic
    data = ds()
    assert torch.equal(data[..., 2], data[..., 4])
    assert torch.equal(data[..., 4], data[..., 18])
    trace = pyro.poutine.trace(ds).get_trace()
    assert trace.nodes["ds-seasons"]["value"].shape == (size, 2)
    assert trace.nodes["dynamic/ds-generated"]["value"].shape == (size, t1)


def test_dynamic_block_creation():
    params = {
        "alpha": {
            "expand": True,
            "domain": constants.real,
            "default": dist.Normal(0.0, 1.0),
        },
        "beta": {
            "expand": True,
            "domain": constants.real,
            "default": dist.Normal(0.0, 1.0),
        },
        "gamma": {
            "expand": True,
            "domain": constants.real,
            "default": dist.Normal(0.0, 1.0),
        },
    }

    def chirp_model(x):
        alpha, beta, gamma = core.name_to_definite(x, "alpha", "beta", "gamma")

        with autoname.scope(prefix=constants.dynamic):
            t = torch.linspace(x.t0, x.t1, x.t1 - x.t0)
            path = pyro.deterministic(
                x.name + "-" + constants.generated,
                (alpha + t * beta + t.pow(2) * gamma).cos(),
            )
        return path

    # we will use gamma, which is defined on runtime block creation
    # it hasn't been defined yet
    with pytest.raises(AttributeError):
        getattr(constants, "gamma")

    sts.register_block(
        "Chirp",
        params,
        chirp_model,
    )

    t1 = 10
    size = 2
    c = sts.Chirp(t1=t1, size=size)
    rw = sts.RandomWalk(t1=t1, size=size)

    data = (c * rw)()
    assert data.shape == (size, 10)
    sts.redefine(c, constants.gamma, rw)  # now it's defined
    assert c().shape == (size, 10)


def test_composition():
    t1 = 10
    size = 2
    c = sts.Chirp(t1=t1, size=size)
    rw = sts.RandomWalk(t1=t1, size=size)

    # make a nontrivial graph
    sde = sts.CCSDE(t1=t1, size=size, loc=c, scale=rw.softplus()) + c
    # we can only call fit if we use dag interpretation; otherwise pyro has
    # more than one rv with same name
    util.set_cache_mode(sde, True)
    assert sde().shape == (size, 10)
    data = sde()
    lik = sts.GaussianNoise(sde, t1=t1, size=size, data=data)
    lik.fit(method_kwargs={"niter": 10})

    util.set_cache_mode(lik, False)
    data = sde()
    lik.data = data
    # now calling fit will fail
    with pytest.raises(RuntimeError):
        lik.fit(method_kwargs={"niter": 10})
