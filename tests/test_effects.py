import pytest
import torch

from stsb3 import core, sts, constants, util, effects


def test_forecast_effect():
    t0 = 3
    t1 = 50
    size = 4
    rw = sts.RandomWalk(t0=t0, t1=t1, size=size, name="rw")
    util.set_cache_mode(rw, True)
    assert rw().shape == (size, t1 - t0)

    lik = sts.GaussianNoise(rw, t0=t0, t1=t1, size=size)
    prior = lik.prior_predictive(nsamples=10)
    assert prior[f"{constants.dynamic}/rw-{constants.generated}"].shape == (
        10,
        size,
        t1 - t0,
    )

    forecast_effect = effects.ForecastEffect(rw, Nt=6)
    with forecast_effect:
        forecasts = torch.stack([rw() for _ in range(10)])
    assert forecasts.shape == (10, size, 6)

    callable_forecast_model = forecast_effect(rw)
    values = callable_forecast_model()
    assert values.shape == (size, 6)
