import collections

import pytest
import torch
import pyro
import pyro.distributions as dist

from stsb3 import core, sts, constants, util


def seed_me(seed=0):
    torch.manual_seed(seed)
    pyro.set_rng_seed(seed)


def test_redefine():
    block = sts.RandomWalk()
    assert core._is_pyro_dist(block.loc)
    core.redefine(block, "loc", torch.tensor(0.1))
    assert type(block.loc) is torch.Tensor


def test_register_address_component():
    with pytest.raises(AttributeError):
        getattr(constants, "new_thing")
    core.register_address_component("new_thing", True, constants.half_line)
    assert hasattr(constants, "new_thing")


def test__is_pyro_dist():
    assert core._is_pyro_dist(dist.Normal(0.0, 1.0))


def test__make_id():
    block = sts.RandomWalk()
    block.name = None  # normally this would be a huge no-no
    name = core._make_id(block, "my_block")
    assert name == "my_block"
    block.name = None
    name = core._make_id(block, None)
    assert "RandomWalk" in name


def test__make_2d():
    a = torch.tensor(0.1)
    b = torch.randn((2,))
    c = torch.randn((10,))
    d = 3.0

    size = 10
    t0 = 0
    t1 = 4

    assert core._make_2d(a, size, t0, t1).shape == (10, 4)
    with pytest.raises(RuntimeError):
        core._make_2d(b, size, t0, t1).shape == (10, 4)
    assert core._make_2d(c, size, t0, t1).shape == (10, 4)
    assert core._make_2d(d, size, t0, t1).shape == (10, 4)


def test__add_fns_to_repr():
    block = sts.RandomWalk()
    str1 = block.__repr__()
    block = block.exp()
    str2 = block.__repr__()
    assert str2 == f"exp({str1})"


def test__obj_name_to_definite():
    block = sts.RandomWalk()
    # default is loc = dist.Normal(0.0, 1.0)
    loc = core._obj_name_to_definite(
        block,
        "loc",
    )
    assert type(loc) == torch.Tensor

    core.redefine(block, "loc", torch.ones(1))
    loc = core._obj_name_to_definite(
        block,
        "loc",
    )
    assert type(loc) == torch.Tensor

    latent = sts.CCSDE()
    core.redefine(block, "loc", latent)
    loc = core._obj_name_to_definite(
        block,
        "loc",
    )
    assert type(loc) == torch.Tensor

    core.redefine(block, "ic", latent)
    with pytest.raises(ValueError):
        core._obj_name_to_definite(block, "ic", flag_on_block=True)


def test_forecast_basics():
    size = 3
    latent = sts.CCSDE(size=size)
    block = sts.RandomWalk(size=size, scale=latent.softplus())
    predictive = pyro.infer.Predictive(block, num_samples=10)
    samples = predictive()

    nsamples = 2
    Nt = 10

    seed_me()
    f1 = core.forecast(latent, samples, Nt=Nt, nsamples=nsamples)
    assert f1.shape == (nsamples, size, Nt)
    seed_me()
    f2 = core.forecast(block, samples, Nt=Nt, nsamples=nsamples)
    assert f2.shape == (nsamples, size, Nt)
    # ensures they're significantly different -- they're different rvs!!!
    assert not torch.equal(f1, f2)


def test__forecast_is_marginalized_var():
    assert core._forecast_is_marginalized_var(f"{constants.dynamic}/thing-stuff")
    assert core._forecast_is_marginalized_var("obs-x")
    assert core._forecast_is_marginalized_var(f"{constants.likelihood}/stuff")


def test_model_basics():
    block = sts.RandomWalk(
        name="rw",
        t1=9,
    )
    assert type(block).num >= 0

    vals = block()
    util.set_cache_mode(block, True)
    vals2 = block()
    vals3 = block()
    assert not torch.equal(vals, vals2)
    # check memoization
    assert torch.equal(vals2, vals3)
    util.set_cache_mode(block, False)
    util.clear_cache(block)
    assert block.cache is None


def test_add_blocks():
    b1 = sts.CCSDE()
    b2 = sts.GlobalTrend()
    util.set_cache_mode(b1, True)
    util.set_cache_mode(b2, True)
    c = b1 + b2
    assert type(c) == core.AddBlock
    assert torch.equal(c(), b1() + b2())


def test_noise_block_basics():
    method_kwargs = {
        "niter": 3,
        "lr": 0.01,
        "loss": "Trace_ELBO",
        "optim": "AdamW",
    }
    for method in ["advi", "low_rank", "nf_block_ar"]:
        b1 = sts.CCSDE()
        data = b1()
        noise = sts.GaussianNoise(b1, data=data, name="lik")
        noise.fit(method=method, method_kwargs=method_kwargs)
        assert noise.guide is not None

    with pytest.raises(NotImplementedError):
        b1 = sts.CCSDE()
        data = b1()
        noise = sts.GaussianNoise(b1, data=data)
        noise.fit(method="not_here", method_kwargs=method_kwargs)

    # prior predictive
    b1 = sts.CCSDE()
    data = b1()
    noise = sts.GaussianNoise(b1, name="lik", data=None)
    pr_p = noise.prior_predictive(nsamples=3)
    assert pr_p[f"lik-{constants.obs}"].shape == (3, 1, 2)

    noise.data = data
    noise.sample(nsamples=3, burnin=0, thin=1.0)

    with pytest.raises(ValueError):
        pr_p = noise.prior_predictive(nsamples=3)
    po_p = noise.posterior_predictive(nsamples=3)
    assert po_p[f"lik-{constants.obs}"].shape == (3, 1, 2)


def test_operations():
    t1 = 5
    size = 3
    rw = sts.RandomWalk(t1=t1, size=size)
    c = sts.CCSDE(t1=t1, size=size)
    season = sts.GlobalTrend(t1=t1, size=size).sin()
    util.set_cache_mode(rw, True)
    util.set_cache_mode(c, True)
    util.set_cache_mode(season, True)

    sum1 = rw + c + season
    sum2 = rw
    for b in [c, season]:
        sum2 += b
    sum3 = sum([rw, c, season])

    assert torch.equal(sum1(), sum2())
    assert torch.equal(sum2(), sum3())

    mul1 = rw * c * season
    mul2 = c
    for b in [rw, season]:
        mul2 *= b

    assert torch.equal(mul1(), mul2())


def test_collect():
    t1 = 20
    size = 2

    # construct a nontrivial model
    loc = sts.AR1(t1=t1, size=size, name="loc")
    util.set_cache_mode(loc, True)
    dgp1 = sts.RandomWalk(t1=t1, size=size, name="dgp1", loc=loc)
    lik1 = sts.GaussianNoise(dgp1.softplus(), t1=t1, size=size, name="lik1")

    dgp2 = sts.RandomWalk(t1=t1, size=size, name="dgp2", loc=loc)
    lik2 = sts.GaussianNoise(dgp2, t1=t1, size=size, name="lik2")

    model = core.collect(lik1, lik2)

    # assert functionality is there
    model.fit(method_kwargs=dict(niter=2))
    # assert we can trace as expected
    trace = pyro.poutine.trace(model).get_trace()
    # get the graph
    graph = util.get_graph_from_root(model)
    assert graph == collections.OrderedDict(
        {
            "likelihood_collection": ["lik1", "lik2"],
            "lik1": ["dgp1"],
            "lik2": ["dgp2"],
            "dgp1": ["loc"],
            "dgp2": ["loc"],
            "loc": [],
        }
    )
