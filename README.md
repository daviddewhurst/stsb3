# `stsb3`

`stsb3` is a library for white-box, composable structural time series modeling and forecasting. `stsb` stands for "structural time series blocks" because the library contains building blocks from which you can construct expressive models. The `3` is because this is the third time I have recreated this library; each time represents an incremental improvement. [Blocks](https://arxiv.org/abs/2009.06865) are collections of random variables that together describe a time series model that ranges from abstract to concrete.


## Installing

Clone the repo, `cd` into the repo, and `pip install .`
Note that python 3.7 is supported, but currently higher versions of python are not guaranteed to work with `stsb3`.


## Documentation

See documentation [here](https://davidrushingdewhurst.com/stsb3/). 


## What's happening?

See [the examples](./example/walkthrough.md).
